using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{


    public GameObject gearX;
    public GameObject gearY;
    public GameObject gearZ;
    public float angleOfRotation;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        gearX.transform.Rotate(angleOfRotation * Vector3.left, Space.World);
        gearY.transform.Rotate(angleOfRotation * Vector3.up, Space.World);
        gearZ.transform.Rotate(angleOfRotation * Vector3.forward, Space.World);
    }

}