using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{

    public GameObject sub;

    // Start is called before the first frame update https://www.youtube.com/watch?v=XjBMU1B0K54
    public CharacterController controller;
    public float speed = 10f;
    public float gravity = -9.8f;
    public float jumpHeight = 3;
    public Transform groundCheck;
    public float groundDistance = 0.3f;
    public LayerMask groundMask;
    Vector3 velocity;
    bool isGrounded;

    void Start()
    {
        sub.SetActive(true);
        StartCoroutine(ExecuteAfterTime(10));
    }

    // Update is called once per frame
    void Update()
    {

        isGrounded= Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        //Debug.Log(isGrounded);
        if(isGrounded && velocity.y > 0)
        {
            velocity.y = -2f;
        }




        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        Vector3 move = transform.right * x + transform.forward * z;
        controller.Move(move * speed * Time.deltaTime);

        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity*Time.deltaTime);
        
    }

        IEnumerator ExecuteAfterTime(float time)
 {
     yield return new WaitForSeconds(time);
     sub.SetActive(false);
     
 }

}
