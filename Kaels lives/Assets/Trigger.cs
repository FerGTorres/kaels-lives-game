using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Trigger : MonoBehaviour
{
    public GameObject sub;
    public GameObject reproductor;

    void Start()
    {
        reproductor.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        reproductor.SetActive(true);
        sub.SetActive(true);
        StartCoroutine(ExecuteAfterTime(20));
        //gameObject.SetActive(false);
        //Destroy(other.gameObject);
    }

    IEnumerator ExecuteAfterTime(float time)
 {
     yield return new WaitForSeconds(time);
     sub.SetActive(false);
     gameObject.SetActive(false);
     
 }




}
