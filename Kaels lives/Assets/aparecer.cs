using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aparecer : MonoBehaviour
{
    
    public GameObject text1;
    public GameObject text2;
    public GameObject button;
    void Start()
    {
    text1.SetActive(false);
     text2.SetActive(false);
     button.SetActive(false);
        StartCoroutine(ExecuteAfterTime(45));
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

            IEnumerator ExecuteAfterTime(float time)
 {
     yield return new WaitForSeconds(time);
     text1.SetActive(true);
     text2.SetActive(true);
     button.SetActive(true);
     
 }
}
